# Jerome's Portfolio
This Gitlab repository is made by **Jerome Emmanuel**

## Pipeline and Coverage
[![coverage report](https://gitlab.com/jeromeemmanuel/web-design-and-programming-lab-10/badges/master/coverage.svg)](https://gitlab.com/jeromeemmanuel/jeromesportofilo/commits/master)

## URL
To access the website, go to [https://jeromez.herokuapp.com](https://jeromez.herokuapp.com)
